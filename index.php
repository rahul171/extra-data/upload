<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Upload your files</title>
  <link rel="stylesheet" href="main.css" />
  <script src="jquery-3.2.1.min.js"></script>
  <script src="main.js"></script>
</head>
<body>
  <form enctype="multipart/form-data" method="POST">
    <p>Upload your file</p>
    <input id="file" type="file"></input><br/>
    <input type="submit" value="Upload"></input>
  </form>

  <div class="status-bar-wrapper">
  	<div class="status-bar-background">
  		<div class="status-bar"></div>
  	</div>
  	<div class="status-bar-text">
  		<span class="status-bar-percent"></span>
  	</div>
  </div>

  <div class="status-text"></div>
</body>
</html>