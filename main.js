$(document).ready(() => {
	$('form').submit((e) => {
		e.preventDefault();

		var fd = new FormData();    
		fd.append('uploaded_file', $('#file')[0].files[0]);

		$('.status-text').text('Uploading...');
		
		$.ajax({
		  xhr: function() {
		    var xhr = new window.XMLHttpRequest();

		    xhr.upload.addEventListener("progress", function(evt) {
		      if (evt.lengthComputable) {
		        var percentComplete = evt.loaded / evt.total;
		        percentComplete = parseInt(percentComplete * 100);

		        $('.status-bar').css('width', percentComplete + '%');
		        $('.status-bar-percent').text(percentComplete + '%');
		      }
		    }, false);

		    return xhr;
		  },
		  url: 'upload.php',
		  type: "POST",
		  data: fd,
		  processData: false,
		  contentType: false,
		  success: function(result) {
		    $('.status-text').text(result);
		  }
		});
	});
});