`uploads/` is writable by web server, for write to work, make the owner of this folder
same as httpd/nginx process owner and set this folder's permission to 755
(visit [this](https://stackoverflow.com/questions/8103860/move-uploaded-file-gives-failed-to-open-stream-permission-denied-error-after/8104498#8104498) for more info)
```bash
sudo chown <httpd/nginx process owner> uploads/
sudo chmod 755 uploads/
```

in case owner of httpd and nginx processes are different, then just set permission to 777 for this folder
so all users can write
(visit [this](https://stackoverflow.com/questions/20582507/move-uploaded-file-failed-to-open-stream-and-permission-denied-error/20582730#20582730) for more info)
```bash
sudo chmod 777 uploads/
```